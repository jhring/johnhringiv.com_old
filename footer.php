<footer>
    <div class="container">
        <div class="row">
            <!-- Link List -->
            <div class="col-md-3 md-margin-bottom-40">
                <div class="headline"><h2>External Links</h2></div>
                <ul class="list-unstyled link-list">
                    <!--<li><a href="http://www.randdvt.com/">Research and Development, LLC</a><i class="fa fa-angle-right"></i></li>-->
                    <li><a href="https://www.uvm.edu/storylab/">Computational Story Lab</a><i class="fa fa-angle-right"></i></li>
                    <li><a href="http://www.uvm.edu/~cmplxsys/">UVM Complex Systems Center</a><i class="fa fa-angle-right"></i></li>
                    <li><a href="https://www.mitre.org">The Mitre Corporation</a><i class="fa fa-angle-right"></i></li>
                    <!--<li><a href="https://daviddewhurst.github.io/">David Dewhurst personal site</a><i class="fa fa-angle-right"></i></li>-->
                    <li><a href="https://www.uvm.edu/~vacc/">Vermont Advanced Computing Core</a><i class="fa fa-angle-right"></i></li>
                </ul>
            </div><!--/col-md-3-->
            <!-- End Link List -->

            <!-- Latest Tweets -->
            <div class="col-md-4">
                <div class="headline"><h2>Latest Tweets</h2></div>
                <a class="twitter-timeline" data-width="300" data-height="125" data-chrome="noheader nofooter transparent" data-dnt="true" data-theme="dark" href="https://twitter.com/johnhringiv">Tweets by John Ring</a> 
                <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>
            <!-- End Latest Tweets -->

            <!-- Address -->
            <div class="col-md-5 map-img md-margin-bottom-40">
                <div class="headline"><h2>Contact</h2></div>
                <ul class="list-unstyled">
                    <li> Email: <a href="mailto:john.ring@uvm.edu" class="">john.ring@uvm.edu</a></li>
                    <li> Phone: (716) 418 - 4142</li>
                </ul>
                <h4>GPG Public Key and Fingerprint:</h4>
                <ul class="list-unstyled">
                    <li>469D 8512 0DA1 9084 D3B0  9E0F 1160 68EF F0C9 5FF2</li>
                    <li> <a href="assets/John_Ring_GPG_Public"> John Ring Public Key</a></li>
                </ul>
            </div><!--/col-md-3-->
            <!-- End Address -->
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
                <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
            </a>
            The content of this site is licensed under a
            <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
                Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
            </a>.
        </div>
    </div><!--/copyright-->
</footer>
</body>
</html>