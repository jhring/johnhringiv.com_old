<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <input type="checkbox" id="navbar-toggle-cbox">
            <div class="navbar-header">
                <label for="navbar-toggle-cbox" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </label>
                <a class="navbar-brand" href="index.php">John Ring's Webpage</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <?php
                    print '<li class="';
                    if ($path_parts['filename'] == "index")  {
                        print 'active';
                    }
                    print '">';
                    print '<a href="index.php">Home</a>';
                    print '</li>';
                    
                    print '<li class="';
                    if ($path_parts['filename'] == "about")  {
                        print 'active';
                    }
                    print '">';
                    print '<a href="about.php">About</a>';
                    print '</li>';
                    
                    print '<li class="';
                    if ($path_parts['filename'] == "blog")  {
                        print 'active';
                    }
                    print '">';
                    print '<a href="blog.php">Blog</a>';
                    print '</li>';
                    
                    print '<li class="';
                    if ($path_parts['filename'] == "contact")  {
                        print 'active';
                    }
                    print '">';
                    print '<a href="contact.php">Contact</a>';
                    print '</li>';
                    ?>
                </ul>
                <ul class="navbar-right social-network social-circle">
                    <li><a href="https://facebook.com/johnhringiv" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/johnhringiv" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.linkedin.com/in/johnhringiv" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="https://gitlab.com/users/jhring/projects" class="icoGitlab" title="Gitlab"><i class="fa fa-gitlab"></i></a></li>
                </ul>
            </div><!--/.nav-collapse -->
    </div>
</nav>
