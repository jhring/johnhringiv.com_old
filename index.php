<?php
include "top.php";
?>

<div class="container" role="main">
    <div class="jumbotron container">
        <div class="row">
            <div class="col-md-6">
                <img src="assets/img/john_amanda_strength.jpg" alt="" class="img-rounded">
            </div>
            <div class="col-md-6">
                <p>A Computer Science Ph.D student at the University of Vermont studying complex systems, computational
                    finance, and computer security.</p>
                <div class="col-md-3">
                    <a href="assets/Ring_Resume.pdf" class="btn btn-primary btn-lg" role="button" aria-disabled="false">My Resume</a>
                </div>
            </div>
        </div>
    </div>

    <?php
    include "affiliated.php";
    ?>

</div> <!-- /container -->

<?php
include "footer.php";
?>