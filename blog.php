<?php
include "top.php";
?>

<div class="container" role="main">
    <div class="row vertical-align">
        <div class="col-md-6">
            <img src="assets/img/robo_typing.jpg" alt="" class="img-rounded" height="337" width="337">
        </div>
        <div class="col-md-6 hidden-sm hidden-xs">
            <h1>My Blog</h1>
            <p>
                Here you will find posts about my latest projects,
                research developments, and various interests. 
                Expected topics include aspects of FOSS (Free Open Source Software),
                digital privacy, Android and Sailing. Entries will be tagged 
                at a future data.
            </p>
        </div>
    </div>


    <!-- Main Content -->
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto hidden-md hidden-lg">
            <h1>My Blog</h1>
            <p>
                Here you will find posts about my latest projects,
                research developments, and various interests.
                Expected topics include aspects of FOSS (Free Open Source Software),
                digital privacy, Android and Sailing. Entries will be tagged
                at a future data.
            </p>
        </div>
        <br>
        <div class="col-lg-8 col-md-10 mx-auto">
            <div class="post-preview">
                <a href="installing_tensorflow.php">
                    <h2 class="post-title">
                        Installing Tensorflow and Keras with CUDA GPU acceleration
                    </h2>
                    <h3 class="post-subtitle">
                        On Fedora 27 using the Anaconda Python distribution
                    </h3>
                </a>
                <p class="post-meta">Posted on March 29, 2018</p>
            </div>
            <!--<hr>-->
            <!-- Pager -->
            <!--
            <div class="clearfix">
                <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>
            </div>
            -->
        </div>
    </div>
</div>
<hr>

<?php
include "footer.php";
?>