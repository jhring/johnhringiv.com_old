<?php
include "top.php";
?>

<div class="container" role="main">
    <div class="row vertical-align">
        <div class="col-md-6">
            <img src="assets/img/self_pic.jpg" alt="" class="img-rounded" height="337px" width="337px">
        </div>
        <div class="col-md-6 hidden-sm hidden-xs">
            <h1>A Bit About Me</h1>
            <p>I am pursing a Ph.D in Computer science at the University of Vermont
            where I previously received a B.S in Computer Science. Much of my day
            is spent contemplating my preferred research topics:
            complex systems, computer security and finance. In my
            spare time I am found either out on the water sailing or looking into a computer
            terminal trying to figure out what I just broke.</p>
            <br>
            My resume may be viewed <a href="assets/Ring_Resume.pdf">here</a>
        </div>
    </div>

    <div class="row">
         <div class="col-md-4 hidden-md hidden-lg">
            <h2>A Bit About Me</h2>
             <p>I am pursing a Ph.D in Computer science at the University of Vermont
                 where I previously received a B.S in Computer Science. Much of my day
                 is spent contemplating my preferred research topics:
                 complex systems, computer security and finance. In my
                 spare time I am found either out on the water sailing or looking into a computer
                 terminal trying to figure out what I just broke.</p>
            <br>
            My resume may be viewed <a href="assets/Ring_Resume.pdf">here</a>
        </div>
        <div class="col-md-4">
            <h2>Research Interests</h2>
            <p>My research interests are a blend of computer science theory and
            complex systems. My work has focused on the development of 
            algorithmic trading strategies, understanding the structure of financial
            markets, and using formal verification techniques in software
            defined networks.</p>
        </div>
        <div class="col-md-4">
            <h2>Professional Activity</h2>
            <p>I'm supported by a graduate teaching assistantship, currently assisting with modeling complex systems,
                and programming languages. Additionally I am a Graduate Fellow at The Mitre Corporation where I
                analyze petabyte-level financial data to understand interactions between high frequency trading
                strategies and market micro-structure.
        </div>
        <div class="col-md-4">
            <h2>Miscellanea</h2>
            <p>I am a member of the <a href="https://www.uvm.edu/storylab/">
            Computational Story Lab</a> research group, which is headed by two
            of my advisers,
            <a href="https://www.uvm.edu/~cdanfort/main/home.html">Chris Danforth</a> 
            and <a href="https://www.uvm.edu/pdodds/">Peter Dodds</a>.
            Additionally I am a member of a computer security research group run
            by my adviser <a href="http://www.cs.uvm.edu/~ceskalka/">Chris Skalka</a>.
            </p>
        </div>
    </div>

    <?php
    include "affiliated.php";
    ?>

</div> <!-- /container -->

<?php
include "footer.php";
?>

