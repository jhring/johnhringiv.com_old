<?php
include "top.php";
?>
<article>
    <div class="container">
        <div class="row">
            <div class="post-heading">
                <h1>Installing Tensorflow and Keras with CUDA GPU acceleration</h1>
                <h2 class="subheading"> On Fedora 27 using the Anaconda Python distribution</h2>
            </div>
            <br/>
            <div class="col-lg-8 col-md-10 mx-auto">
                <p>Last week I was setting up my workstation for deep learning and found the process less
                than user friendly than I would have liked so I have compiled the steps I took here in the hope that it
                saves others some aggravation. If you are not using Fedora simply install the Nvidia drivers with your
                distribution's preferred method and skip to the Setting up Anaconda section.</p>
                <h3>Installing the Nvidia Drivers</h3>
                As Fedora does not ship propriety code we will need to add
                <a href="https://negativo17.org/nvidia-driver/">Negativo's excellent Nvidia repository</a>.
                <br/>
                <div>
                    <kbd>sudo dnf config-manager --add-repo=http://negativo17.org/repos/fedora-nvidia.repo</kbd>
                </div>
                With the repository added we may now install the driver, required tools and libraries.<br/>
                <kbd>dnf install kernel-devel dkms-nvidia  nvidia-driver-cuda</kbd><br/>
                <kbd>dnf install cuda-devel cuda-cudnn-devel</kbd><br/>
                After all packages are installed reboot the system so that it may use the new driver.
                <h3>Setting up Anaconda</h3>
                Download and install the Anaconda python distribution. You can find the latest version
                <a href="https://www.anaconda.com/download/#linux">here</a>. Pay attention to the prompts, most users
                will want to let the installer modify their dotfiles.<br/>
                <kbd>wget https://repo.continuum.io/archive/Anaconda3-5.1.0-Linux-x86_64.sh</kbd><br/>
                <kbd>bash Anaconda3-5.1.0-Linux-x86_64.sh</kbd><br>

                Finally install Tensorflow and Keras. Note the order of the commands, if <var>tensorflow-gpu</var> is
                installed before Keras then Keras will not use the GPU for unknown reasons. <br>
                <kbd>conda update conda</kbd><br>
                <kbd>conda update anaconda</kbd><br>
                <kbd>conda install keras-gpu</kbd><br>
                <kbd>conda install tensorflow-gpu</kbd> <br><br>

                You should now have Keras and Tensorflow installed with GPU support. Please feel free to contact me
                if you encounter any issues or have suggestions on how to improve the guide.
                <br/><br/>
                <span class="meta">Posted on March 29, 2018</span> <br>
            </div>
        </div>
    </div>
</article>

<?php
include "footer.php";
?>
