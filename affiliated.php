<!-- .hidden-sm-down will be available in bootstrap 4 -->
<div class="headline hidden-sm hidden-xs">
    <h2>Affiliated Organizations</h2>
</div>
<div>
    <div class="col-md-3 hidden-sm hidden-xs">
        <a href="http://www.uvm.edu/~cmplxsys/">
            <img class="img-responsive" src="assets/img/affiliated/roboctopus.png" alt="Vermont Complex Systems Center">
        </a>
    </div>
    <div class="col-md-3 hidden-sm hidden-xs">
        <a href="https://www.mitre.org/">
            <img class="img-responsive" src="assets/img/affiliated/Mitre_Corporation_logo.png" alt="Mitre Corporation">
        </a>
    </div>

    <!--
    <div class="col-md-3 hidden-sm hidden-xs">
        <a href="http://www.uvm.edu/~vacc/">
            <img class="img-responsive" src="assets/img/affiliated/VACC.png" alt="Vermont Advanced Computing Core">
        </a>
    </div>
    -->
    <div class="col-md-3 hidden-sm hidden-xs">
        <a href="https://www.uvm.edu/">
            <img src="assets/img/affiliated/uvmlogo.png" alt="University of Vermont">
        </a>
    </div>
</div>