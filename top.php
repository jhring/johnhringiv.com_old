<!DOCTYPE html>
<html lang="en">
    <head>
        <title>John H. Ring IV's Personal Site</title>
        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="John H. Ring IV's Web Site">
        <meta name="author" content="John H. Ring IV">

        <!-- Bootstrap and Font Awesome -->
        <!--
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
        
        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/slate/bootstrap.min.css" rel="stylesheet" integrity="sha384-RpX8okQqCyUNG7PlOYNybyJXYTtGQH+7rIKiVvg1DLg6jahLEk47VvpUyS+E2/uJ" crossorigin="anonymous">
        
        <link href="assets/css/custom.css" rel="stylesheet">
    </head>
    
    <?php
    $domain = "//";
    $server = htmlentities($_SERVER['SERVER_NAME'], ENT_QUOTES, "UTF-8");
    $domain .= $server;
    $phpSelf = htmlentities($_SERVER['PHP_SELF'], ENT_QUOTES, "UTF-8");         
    $path_parts = pathinfo($phpSelf);

    require_once('lib/security.php');
    if ($path_parts['filename'] == "contact") {
        include "lib/validation-functions.php";
        include "lib/mail-message.php";
    }

    print '<body id="' . $path_parts['filename'] . '">';
    include "nav.php";
    ?>
