<?php
include "top.php";
?>

<?php
$thisURL = $domain . $phpSelf;
$name = "";
$email = "";
$subject = "";
$msg = "";
$file = "";
$firstMistake = "";
$nameErr = false;
$emailErr = false;
$subjectErr = false;
$msgErr = false;

$errorMsg = array();
$mailed=false;

if (isset($_POST["btnSubmit"])) {
    if (!securityCheck($thisURL)) {
        $out = "<p>Sorry you cannot access this page. ";
        $out.= "Security breach detected and reported.</p>";
        die($out);
    }

    $name = htmlentities($_POST["InputName"], ENT_QUOTES, "UTF-8");

    $email = htmlentities($_POST["InputEmail"], ENT_QUOTES, "UTF-8");

    $msg = htmlentities($_POST["InputMsg"], ENT_QUOTES, "UTF-8");

    $subject = htmlentities($_POST["InputSubject"], ENT_QUOTES, "UTF-8");

    $file = htmlentities($_POST["InputFile"], ENT_QUOTES, "UTF-8");

    if ($name == "") {
        $errorMsg[] = "Please enter your name";
        $nameErr = true;
    } elseif (!verifyAlphaNum($name)) {
        $errorMsg[] = "Invalid character in name";
        $nameErr = true;
    }
    if ($firstMistake == "" and $nameErr) {
        $firstMistake = "Name";
    }

    if ($email == "") {
        $errorMsg[] = "Please enter your email";
        $emailErr = true;
    } elseif (!verifyEmail($email)) {
        $errorMsg[] = "Incorrect email address";
        $emailErr = true;
    }
    if ($firstMistake == "" and $nameErr) {
        $firstMistake = "Email";
    }

    if ($msg == "") {
        $errorMsg[] = "Message cannot be blank";
        $msgErr = true;
    }

    if ($firstMistake == "" and $msgErr) {
        $firstMistake = "Msg";
    }

    if ($subject == "") {
        $errorMsg[] = "Please select a subject";
        $subjectErr = true;

        if ($firstMistake == "") {
            $firstMistake = "subject";
        }
    }
    $message = '<h2>Thank you for your message</h2>';
    $message .= '<p>You can expect a reply shortly.</p><br>';
    $message .= '<h3>Your information.</h3>';
    foreach ($_POST as $htmlName => $value) {
        if ($htmlName != 'btnSubmit') {
            $message .= "<p>";
            // remove input prefix
            $camelCase = preg_split('/(?=[A-Z])/', substr($htmlName, 5));
            foreach ($camelCase as $oneWord) {
                $message .= $oneWord . " ";
            }
            $message .= " = " . htmlentities($value, ENT_QUOTES, "UTF-8") . "</p>";
        }
    }

    if (!$errorMsg) {
        $to = $email; // the person who filled out the form
        $cc = "";
        $bcc = 'jhring@uvm.edu';
        $from = $name . " <form@johnhringiv.com>";
        // subject of mail should make sense to your form
        $todaysDate = strftime("%x");
        $subject .= ' ' . $todaysDate;
        $mailed = sendMail($to, $cc, $bcc, $from, $subject, $message);
    }
}
?>

<div class="container" role="main">
    <div class="row vertical-align">
        <div class="col-md-6">
            <img src="assets/img/plat_typing.jpg" alt="" class="img-rounded" height="377" width="377">
        </div>
        <div class="col-md-6 hidden-sm hidden-xs">
            <h1>Contact</h1>
            <p>
                You may get in touch with me using the form below. Alternatively
                I may be reached via my email <a href="mailto:john.ring@uvm.edu">john.ring@uvm.edu</a>
                or phone (716) 418-4142. Prefer encrypted communication? So do I!
                You may use the <a href="https://whispersystems.org/">Signal</a> app
                to reach me via phone or email me with pgp, <a href="assets/John_Ring_GPG_Public"> my public key</a>.
            </p>
        </div>
    </div> <br>

    <?php
    if (isset($_POST["btnSubmit"]) AND empty($errorMsg)) {
        if ($mailed){
            print "<h2>Thank you for providing your information.</h2>";
            print "<p>For your records a copy of this data has ";
            print "been sent:</p>";
            print "<p>To: " . $email . "</p>";
        } else {
            print "<h2>An error occurred</h2>";
            print "<p>Please use one of the alternative contact methods above.</p>";
        }
    } else {
        //print '<h2>Contact Me</h2>';
        if($errorMsg) {
            print '<div id="errors">' . "\n";
            print "<h2>Your form has the following mistakes that need to be fixed.</h2>\n";
            print "<ol>\n";
            foreach ($errorMsg as $err) {
                print "<li>" . $err . "</li>\n";
            }
            print "</ol>\n";
            print "</div>\n";
        }
    ?>

    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"
            id="frmRegister"
            method="post"
            class="contact_form">
        <div class="form-group <?php if ($nameErr) print ' mistake'; ?>">
            <label for="Name">Name</label>
            <input <?php if ($firstMistake == "name" or $firstMistake == "") print ' autofocus '; ?>
                    class="form-control"
                    type="text"
                    id="InputName"
                    name="InputName"
                    maxlength="45"
                    onfocus="this.select()"
                    placeholder="Enter name"
                    value="<?php print $name; ?>"
                    tabindex="100">
        </div>
        <div class="form-group <?php if ($emailErr) print ' mistake'; ?>">
            <label for="InputEmail">Email address</label>
            <input <?php if ($firstMistake == "email") print ' autofocus '; ?>
                    type="email"
                    class="form-control"
                    id="InputEmail"
                    name="InputEmail"
                    onfocus="this.select()"
                    tabindex="120"
                    value="<?php print $email; ?>"
                    aria-describedby="emailHelp"
                    placeholder="Enter email"
                    maxlength="45">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group <?php if ($subjectErr) print ' mistake'; ?>">
            <label for="InputSubject">Subject</label>
            <select <?php if ($firstMistake == "subject") print ' autofocus '; ?>
                    id="InputSubject"
                    name="InputSubject"
                    tabindex="130">
                <option <?php if($subject=="") print " selected "; ?>
                value = "">Please Select</option>
                <option <?php if($subject=="contract") print " selected "; ?>
                        value="contract">Contract Work</option>
                <option <?php if($subject=="project") print " selected "; ?>
                        value="project">Project</option>
                <option <?php if($subject=="publication") print " selected "; ?>
                        value="publication">Publication</option>
                <option <?php if($subject=="research") print " selected "; ?>
                        value="research">Research</option>
                <option <?php if($subject=="other") print " selected "; ?>
                        value="other">Other</option>
            </select>
        </div>
        <div class="form-group <?php if ($msgErr) print ' mistake'; ?>">
            <label for="InputMsg">Message</label>
            <textarea <?php if ($firstMistake == "msg") print ' autofocus '; ?> class="form-control" id="InputMsg" name="InputMsg" rows="3" tabindex="140"><?php print $msg; ?></textarea>
        </div>

        <button type="submit"
                id="btnSubmit"
                name="btnSubmit"
                tabindex="900"
                class="btn btn-primary">Submit</button>
    </form> <br>

</div>

<?php } // end body submit
?>

<?php
include "footer.php";
?>